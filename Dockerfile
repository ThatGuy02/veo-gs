FROM node:10.12.0-stretch

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install --only=production

COPY . .

EXPOSE 8080

ENTRYPOINT [ "node", "bin/veo-gs.js", "--listenIp=0.0.0.0" ]
