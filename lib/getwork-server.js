/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of veo-gs.

veo-gs is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

veo-gs is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with veo-gs.  If not, see <http://www.gnu.org/licenses/>.
*/
const http = require('http');
const GetWorkMessageHandler = require('./getwork-message-handler');
const logger = require('./logger-factory').createLogger('GetWorkServer');

function generateId(a) {
	return a ? (a^Math.random()*16>>a/4).toString(16):([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, generateId);
}

class GetWorkServer {
	constructor(ignoreNoWorkAvailable, immediateReturnOnSubmit, listenIpAddress, listenPort, remoteHost, remotePort, route) {
		this._listenIpAddress = listenIpAddress;
		this._listenPort = listenPort;
		this._messageHandler = new GetWorkMessageHandler(ignoreNoWorkAvailable, immediateReturnOnSubmit, remoteHost, remotePort);
		this._route = route;
	}

	start() {
		const self = this;

		self._server = http.createServer((req, res) => {
			req.on('error', (err) => {
				logger.warn('Request error in GetWorkServer: ' + err);

				res.writeHead(500);
				res.end();
			});
			res.on('error', (err) => {
				logger.error('Response error in GetWorkServer: ' + err);
			});

			if (req.method === 'POST' && req.url === self._route) {
				const bodyChunks = [];
				const requestId = generateId();

				req.on('data', (chunk) => {
					bodyChunks.push(chunk);
				}).on('end', () => {
					const bodyJson = Buffer.concat(bodyChunks).toString();

					logger.debug(`Received request with id ${requestId}: ${bodyJson}`);

					try {
						const body = JSON.parse(bodyJson);

						self._messageHandler.handleWorkRequest(body, (messageResult) => {
							if (messageResult.statusCode === 200) {
								res.writeHead(200, {'Content-Type': 'application/octet-stream'});
								res.write(messageResult.response);

								logger.debug(`Sending success response for request with id ${requestId}: ${messageResult.response}`);
							} else {
								if (messageResult.statusCode === 503) {
									logger.info(`Stratum connection unavailable for request with id ${requestId}`);
								}

								logger.warn(`Unable to handle request with id ${requestId}: ${bodyJson}`);

								res.writeHead(500);
							}

							res.end();
						});
					} catch (err) {
						logger.warn(`Error while attempting to handle request ${bodyJson}: ${err}`);

						res.writeHead(500);
						res.end();
					}
				});
			} else {
				res.writeHead(404);
				res.end();
			}
		});

		self._server.on('error', (err) => {
			if (err.code === 'EADDRINUSE') {
				logger.error('Address already in use');
			} else {
				logger.error('Unexpected error: ' + err);
			}
		});

		self._server.listen({
			exclusive: true,
			host: self._listenIpAddress,
			port: self._listenPort
		}, () => {
			logger.info(`Listening on http://${self._listenIpAddress}:${self._listenPort}${self._route}`);
		});
	}
}

module.exports = GetWorkServer;