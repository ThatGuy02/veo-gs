/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of veo-gs.

veo-gs is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

veo-gs is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with veo-gs.  If not, see <http://www.gnu.org/licenses/>.
*/
const winston = require('winston');
require('winston-daily-rotate-file');

const formatter = winston.format.printf((info) => `${info.timestamp} [${info.label}][${info.level}]: ${info.message}`);

class LoggerFactory {
	constructor() {
		this._loggerInfos = [];
	}

	createLogger(label) {
		const self = this;

		const coloredConsoleLogTransport = new (winston.transports.Console)({
			colorize: true,
			level: 'debug'
		});

		const logger = winston.createLogger({
			exitOnError: false,
			format: winston.format.combine(winston.format.label({ label: label }), winston.format.timestamp({ format: 'YYYY-MM-DDTHH:mm:ss.SSSZZ' }), formatter),
			transports: [ coloredConsoleLogTransport ]
		});

		self._loggerInfos.push({ label: label, logger: logger });

		return logger;
	}

	reconfigureLoggers(level, logFileName) {
		const self = this;

		const coloredConsoleLogTransport = new (winston.transports.Console)({
			colorize: true,
			level: level
		});
		
		const transports = [ coloredConsoleLogTransport ];
		if (typeof(logFileName) === 'string' && logFileName.length > 0) {
			const rotatingLogFileTransport = new (winston.transports.DailyRotateFile)({
				datePattern: 'YYYY-MM-DD',
				filename: logFileName,
				level: level,
				maxFiles: '7d',
				maxSize: '1g'
			});

			transports.push(rotatingLogFileTransport);
		}

		self._loggerInfos.forEach((loggerInfo) => loggerInfo.logger.configure({
			exitOnError: false,
			format: winston.format.combine(winston.format.label({ label: loggerInfo.label }), winston.format.timestamp({ format: 'YYYY-MM-DDTHH:mm:ss.SSSZZ' }), formatter),
			level: level,
			transports: transports
		}));
	}
}

module.exports = new LoggerFactory();