/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of veo-gs.

veo-gs is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

veo-gs is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with veo-gs.  If not, see <http://www.gnu.org/licenses/>.
*/
const constants = require('./constants');
const logger = require('./logger-factory').createLogger('StratumClientMultiplexer');
const StratumClient = require('./stratum-client');

function createStratumClient(ignoreNoWorkAvailable, minerAddress, remoteHost, remotePort, stratumClients, callback) {
	const stratumClient = new StratumClient(ignoreNoWorkAvailable, minerAddress, remoteHost, remotePort);

	logger.verbose(`Creating client for ${minerAddress}`);
			
	stratumClient.connect((errorOnStartup) => {
		logger.verbose(`Received close event for ${minerAddress}, removing`);

		if (errorOnStartup === false) {
			stratumClients[minerAddress].destroy();

			delete stratumClients[minerAddress];
		}
	}, (errorOnStartup) => {
		if (errorOnStartup === false) {
			stratumClients[minerAddress] = stratumClient;

			callback(stratumClient);
		} else {
			logger.error(`Unable to connect to Stratum for ${minerAddress}`);

			callback(null);
		}
	});
}

class StratumClientMultiplexer {
	constructor(ignoreNoWorkAvailable, remoteHost, remotePort) {
		logger.info(`Using remote host ${remoteHost}:${remotePort}`);

		this._ignoreNoWorkAvailable = ignoreNoWorkAvailable;
		this._remoteHost = remoteHost;
		this._remotePort = remotePort;
		this._stratumClients = {};
	}

	getMiningData(minerAddress, callback) {
		const self = this;

		if (self._stratumClients.hasOwnProperty(minerAddress)) {
			callback(self._stratumClients[minerAddress].getMiningData());
		} else {
			createStratumClient(self._ignoreNoWorkAvailable, minerAddress, self._remoteHost, self._remotePort, self._stratumClients, (stratumClient) => {
				if (stratumClient === null) {
					logger.error(`No client created for ${minerAddress}, returning null mining data`);

					callback(null);
				} else {
					callback(stratumClient.getMiningData());
				}
			});
		}
	}

	submitWork(minerAddress, nonce, callback) {
		const self = this;

		if (self._stratumClients.hasOwnProperty(minerAddress)) {
			self._stratumClients[minerAddress].submitWork(nonce, callback);
		} else {
			createStratumClient(self._ignoreNoWorkAvailable, minerAddress, self._remoteHost, self._remotePort, self._stratumClients, (stratumClient) => {
				if (stratumClient === null) {
					logger.error(`No client created for ${minerAddress}, returning generic submit work error`);

					callback(constants.stratum.RESPONSE_CLIENT_NOT_CREATED);
				} else {
					self._stratumClients[minerAddress].submitWork(nonce, callback);
				}
			});
		}
	}

	submitWorkImmediate(minerAddress, nonce, callback) {
		const self = this;

		if (self._stratumClients.hasOwnProperty(minerAddress)) {
			callback(self._stratumClients[minerAddress].submitWork(nonce));
		} else {
			createStratumClient(self._ignoreNoWorkAvailable, minerAddress, self._remoteHost, self._remotePort, self._stratumClients, (stratumClient) => {
				if (stratumClient === null) {
					logger.error(`No client created for ${minerAddress}, returning generic submit work error`);

					callback(constants.stratum.RESPONSE_CLIENT_NOT_CREATED);
				} else {
					callback(self._stratumClients[minerAddress].submitWork(nonce));
				}
			});
		}
	}
}

module.exports = StratumClientMultiplexer;