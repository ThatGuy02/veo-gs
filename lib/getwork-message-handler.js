/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of veo-gs.

veo-gs is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

veo-gs is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with veo-gs.  If not, see <http://www.gnu.org/licenses/>.
*/
const constants = require('./constants');
const StratumClientMultiplexer = require('./stratum-client-multiplexer');

function handleGetWorkRequest(stratumClientMultiplexer, minerAddress, callback) {
	stratumClientMultiplexer.getMiningData(minerAddress, (miningData) => {
		if (miningData === null || typeof(miningData.blockHash) !== 'string') {
			callback({ statusCode: 503 });
		} else {
			const miningDataMessage = `["ok",[-6,"${miningData.blockHash}",${miningData.jobDifficulty},${miningData.jobDifficulty}]]`;
		
			callback({ response: miningDataMessage, statusCode: 200 });
		}
	});
}

function handleSubmitWorkRequest(stratumClientMultiplexer, minerAddress, nonce, callback) {
	stratumClientMultiplexer.submitWork(minerAddress, nonce, (submitResult) => {
		if (submitResult.hasOwnProperty('result') && submitResult.result.acc === 1) {
			callback({ response: constants.getWork.RESPONSE_VALID_SHARE, statusCode: 200 });
		} else {
			switch (submitResult.error.code) {
				case constants.getWork.ERROR_ID_INVALID_SHARE:
					callback({ response: constants.getWork.RESPONSE_INVALID_SHARE, statusCode: 200 });
				break;

				case constants.getWork.ERROR_ID_LOW_DIFFICULTY_SHARE:
					callback({ response: constants.getWork.RESPONSE_LOW_DIFFICULTY_SHARE, statusCode: 200 });
				break;

				case constants.stratum.RESPONSE_CLIENT_NOT_CREATED.error.code:
					callback({ statusCode: 503 });
				break;

				default:
					callback({ statusCode: 500 });
				break;
			}
		}
	});
}

function handleSubmitWorkRequestImmediate(stratumClientMultiplexer, minerAddress, nonce, callback) {
	stratumClientMultiplexer.submitWorkImmediate(minerAddress, nonce, (submitResult) => {
		if (submitResult === true) {
			callback({ response: constants.getWork.RESPONSE_VALID_SHARE, statusCode: 200 });
		} else {
			callback({ statusCode: 500 });
		}
	});
}

class GetWorkMessageHandler {
	constructor(ignoreNoWorkAvailable, immediateReturnOnSubmit, remoteHost, remotePort) {
		this._stratumClientMultiplexer = new StratumClientMultiplexer(ignoreNoWorkAvailable, remoteHost, remotePort);
		this._submitWorkImpl = immediateReturnOnSubmit === true ? handleSubmitWorkRequestImmediate : handleSubmitWorkRequest;
	}

	handleWorkRequest(body, callback) {
		const self = this;

		if (!Array.isArray(body)) {
			callback({ statusCode: 500 });
		} else if (body.length === 2 && body[0] === 'mining_data') {
			handleGetWorkRequest(self._stratumClientMultiplexer, body[1], callback);
		} else if (body.length === 3 && body[0] === 'work') {
			self._submitWorkImpl(self._stratumClientMultiplexer, body[2], body[1], callback);
		} else {
			callback({ statusCode: 500 });
		}
	}
}

module.exports = GetWorkMessageHandler;