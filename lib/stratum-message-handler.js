/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of veo-gs.

veo-gs is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

veo-gs is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with veo-gs.  If not, see <http://www.gnu.org/licenses/>.
*/
const constants = require('./constants');
const logger = require('./logger-factory').createLogger('StratumMessageHandler');

function handleServerNotificationMessage(client, message) {
	switch (message.method) {
		case constants.stratum.METHOD_ID_NEW_BLOCK_HASH:
			if (message.hasOwnProperty('error')) {
				if (client.ignoreNoWorkAvailable === true) {
					logger.debug(`Received no work available message for ${client.minerAddress}, ignoring`);
				} else {
					logger.debug(`Received no work available message for ${client.minerAddress}`);

					client.updateBlockHash(null);
				}
			} else {
				client.updateBlockHash(message.result.bHash);
			}
		break;

		case constants.stratum.METHOD_ID_NEW_JOB_DIFFICULTY:
			client.updateJobDifficulty(message.result.jDiff);
		break;

		default:
			logger.warn(`Unknown method from server for ${client.minerAddress}: ${message.method}`);
		break;
	}
}

function handleServerResponseMessage(client, message) {
	switch (message.id) {
		case constants.stratum.MESSAGE_ID_SUBSCRIBE:
			client.initializeMiningData(message);
		break;

		default:
			if (message.id < constants.stratum.MESSAGE_ID_SUBMIT_START) {
				logger.warn(`Unknown message id from server for ${client.minerAddress}: ${message.id}`);
			} else {
				client.handleSubmitWorkResult(message);
			}
		break;
	}
}

class StratumMessageHandler {
	handleServerMessage(client, messageJson) {
		try {
			const message = JSON.parse(messageJson);
	
			if (message.hasOwnProperty('method')) {
				handleServerNotificationMessage(client, message);
			} else if (message.hasOwnProperty('id')) {
				handleServerResponseMessage(client, message);
			} else {
				logger.warn(`Unknown message from server for ${client.minerAddress}: ${message}`);
			}
		} catch (err) {
			logger.warn(`Error while attempting to handle message ${messageJson}: ${err}`);
		}
	}
}

module.exports = new StratumMessageHandler();