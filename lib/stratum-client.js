/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of veo-gs.

veo-gs is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

veo-gs is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with veo-gs.  If not, see <http://www.gnu.org/licenses/>.
*/
const net = require('net');
const constants = require('./constants');
const logger = require('./logger-factory').createLogger('StratumClient');
const stratumMessageHandler = require('./stratum-message-handler');

class StratumClient {
	constructor(ignoreNoWorkAvailable, minerAddress, remoteHost, remotePort) {
		this.ignoreNoWorkAvailable = ignoreNoWorkAvailable;
		this.minerAddress = minerAddress;

		this._buffer = '';
		this._isReady = false;
		this._isStartupError = true;
		this._miningData = null;
		this._nextSubmitId = constants.stratum.MESSAGE_ID_SUBMIT_START;
		this._remoteHost = remoteHost;
		this._remotePort = remotePort;
		this._socket = new net.Socket();
		this._submitCallbacks = {};
	}

	connect(onClose, callback) {
		const self = this;

		self._socket.on('close', () => {
			self._isReady = false;

			logger.verbose(`Client closed for ${self.minerAddress}`);

			onClose(self._isStartupError);
		}).on('data', (chunk) => {
			self._buffer += chunk;
			let i = 0;

			while ((i = self._buffer.indexOf('\n')) > -1) {
				const messageJson = self._buffer.slice(0, i);

				logger.debug(`Client received message for ${self.minerAddress}: ${messageJson}`);
				
				stratumMessageHandler.handleServerMessage(self, messageJson);

				self._buffer = self._buffer.slice(i + 1);
			}
		}).on('end', () => {
			self._isReady = false;

			logger.verbose(`Server closed connection for ${self.minerAddress}`);
		}).on('error', (err) => {
			self._isReady = false;

			logger.warn(`Client error for ${self.minerAddress}: ${err}`);

			if (self._isStartupError === true) {
				callback(self._isStartupError);
			}
		}).on('ready', () => {
			self._isReady = true;

			logger.verbose(`Client connection ready for ${self.minerAddress}`);

			const subscribeMessage = `{"id":${constants.stratum.MESSAGE_ID_SUBSCRIBE},"method":${constants.stratum.METHOD_ID_SUBSCRIBE},"params":{"id":"${self.minerAddress}"}}\n`;

			self._socket.write(subscribeMessage);

			callback(self._isStartupError);
		});

		self._socket.connect(self._remotePort, self._remoteHost, () => {
			self._isStartupError = false;

			logger.verbose(`Opened client connection for ${self.minerAddress} to ${self._remoteHost}:${self._remotePort}`);
		});
	}

	destroy() {
		const self = this;

		self._isReady = false;
		self._socket.destroy();

		logger.verbose(`Destroyed client connection for ${self.minerAddress}`);
	}

	getMiningData() {
		const self = this;

		if (!self.isReady()) {
			return null;
		}

		return self._miningData;
	}

	handleSubmitWorkResult(message) {
		const self = this;

		if (self._submitCallbacks.hasOwnProperty(message.id)) {
			logger.verbose(`Executing callback for submit id ${message.id} for ${self.minerAddress}`);

			self._submitCallbacks[message.id](message);

			delete self._submitCallbacks[message.id];
		} else {
			logger.warn(`No callback found for submit id ${message.id} for ${self.minerAddress}`);
		}
	}

	initializeMiningData(message) {
		const self = this;

		self._miningData = {};

		self.updateBlockHash(message.result.bHash);
		self.updateJobDifficulty(message.result.jDiff);
	}

	isReady() {
		const self = this;

		return self._isReady;
	}

	submitWork(nonce, callback) {
		const self = this;

		const immediateSubmit = typeof(callback) !== 'function';

		if (!self.isReady()) {
			logger.warn(`Client not ready for ${minerAddress}, returning generic submit work error`);

			if (immediateSubmit === true) {
				return false;
			} else {
				callback(constants.stratum.RESPONSE_CLIENT_NOT_CREATED);
			}
		} else {
			const submitId = self._nextSubmitId++;
			const submitMessage = `{"id":${submitId},"method":${constants.stratum.METHOD_ID_SUBMIT_WORK},"params":{"id":"${self.minerAddress}","nonce":"${nonce}"}}\n`;

			if (immediateSubmit === false) {
				self._submitCallbacks[submitId] = callback;
			} else {
				self._submitCallbacks[submitId] = (submitResult) => {
					logger.verbose(`Received result for submit id ${submitId} for ${self.minerAddress}: ${submitResult}`);
				};
			}

			logger.verbose(`Submitting share ${nonce} for ${self.minerAddress} with id ${submitId}`);

			self._socket.write(submitMessage);

			if (self._nextSubmitId === Number.MAX_SAFE_INTEGER) {
				logger.debug(`Maximum submit id for ${self.minerAddress} reached, resetting to ${constants.stratum.MESSAGE_ID_SUBMIT_START}`);

				self._nextSubmitId = constants.stratum.MESSAGE_ID_SUBMIT_START;
			}

			return true;
		}
	}

	updateBlockHash(blockHash) {
		const self = this;

		logger.debug(`Updating block hash for ${self.minerAddress}: ${blockHash}`);

		self._miningData.blockHash = blockHash;
	}

	updateJobDifficulty(jobDifficulty) {
		const self = this;

		logger.debug(`Updating job difficulty for ${self.minerAddress}: ${jobDifficulty}`);

		self._miningData.jobDifficulty = jobDifficulty;
	}
}

module.exports = StratumClient;