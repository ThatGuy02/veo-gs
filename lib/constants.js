/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of veo-gs.

veo-gs is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

veo-gs is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with veo-gs.  If not, see <http://www.gnu.org/licenses/>.
*/

module.exports = {
	getWork: {
		RESPONSE_INVALID_SHARE: '[-6,105,110,118,97,108,105,100,32,119,111,114,107]',
		RESPONSE_LOW_DIFFICULTY_SHARE: '[-6,108,111,119,32,100,105,102,102]',
		RESPONSE_VALID_SHARE: '[-6,102,111,117,110,100,32,119,111,114,107]'
	},
	stratum: {
		ERROR_ID_INVALID_SHARE: 21,
		ERROR_ID_LOW_DIFFICULTY_SHARE: 23,
		MESSAGE_ID_SUBMIT_START: 2,
		MESSAGE_ID_SUBSCRIBE: 1,
		METHOD_ID_NEW_BLOCK_HASH: 2,
		METHOD_ID_NEW_JOB_DIFFICULTY: 3,
		METHOD_ID_SUBMIT_WORK: 1,
		METHOD_ID_SUBSCRIBE: 0,
		RESPONSE_CLIENT_NOT_CREATED: {
			error: {
				code: -2
			}
		}
	}
};