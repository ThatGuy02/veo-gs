# veo-gs

## Introduction
veo-gs is a tool for connecting the Amoveo GetWork and Stratum protocols. Miner software designed for the HTTP-basd GetWork protocol can mine to a pool using the TCP-based Stratum protocol.

## Requirements
* Node.js

or

* Docker

## Installation using npm
1. Clone the repository using git, or download the repository as an archive.
2. From the root repository folder, run:
```
$ npm install
```
3. To start the proxy with default options, run:
```
$ node bin/veo-gs.js
```
4. To override default options, e.g. logging level:
```
$ node bin/veo-gs.js --logLevel=debug
```

## Installation using Docker
1. Clone the repository using git, or download the repository as an archive.
2. From the root repository folder, run:
```
$ docker build -t veo-gs .
```
3. Start as a service:
```
$ docker run -d --name veo-gs --restart unless-stopped -p 8080:8080 veo-gs
```
4. To override default options, e.g. logging level:
```
$ docker run -d --name veo-gs --restart unless-stopped -p 8080:8080 veo-gs --logLevel=debug
```

## Additional Options
These options can be overriden when starting the proxy. Options are set using "--name=value" format.

### help - Boolean
> Display the help message and exit.

### listenIp - String
> The local IP address to listen on for the GetWork server.

> DEFAULT: 127.0.0.1

### listenPort - Number
> The local port to use for the GetWork server.

> DEFAULT: 8080

### logFile - String
> The file to log to, recommended is something like veo-gs-log-[DATE].log.

> DEFAULT: [NONE]

### logLevel - String
> Logging level, valid levels in increasing severity are debug/verbose/info/warn/error.

> DEFAULT: info

### remoteHost - String
> The remote host name to connect to for Stratum.

> DEFAULT: stratum.amoveopool.com

### remotePort - Number
> The remote port to connect to for Stratum.

> DEFAULT: 8822

### workRoute - String
> The route to use for the GetWork endpoint.

> DEFAULT: /work

## Usage
The default configuration starts the GetWork server on http://127.0.0.1:8080/work and points to stratum.amoveopool.com:8822. These values can all be changed on startup using the above options.