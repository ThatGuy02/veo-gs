#! /usr/bin/env node
/*
Copyright (C) 2018 BikBikBikBikBik

This file is part of veo-gs.

veo-gs is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

veo-gs is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with veo-gs.  If not, see <http://www.gnu.org/licenses/>.
*/
const commandLineArgs = require('command-line-args');
const commandLineUsage = require('command-line-usage');
const GetWorkServer = require('../lib/getwork-server');
const loggerFactory = require('../lib/logger-factory');

const commandLineOptions = [
	{
		name: 'help',
		alias: 'h',
		type: Boolean,
		description: 'Display this message.'
	},
	{
		name: 'listenIp',
		alias: 'l',
		type: String,
		typeLabel: '{underline ListenIpAddress}',
		description: 'The local IP address to listen on for the GetWork server.'
	},
	{
		name: 'listenPort',
		alias: 'p',
		type: Number,
		typeLabel: '{underline Port}',
		description: 'The local port to use for the GetWork server.'
	},
	{
		name: 'logFile',
		alias: 'f',
		type: String,
		typeLabel: '{underline LogFile}',
		description: 'The file to log to, recommended is something like veo-gs-log-[DATE].log.'
	},
	{
		name: 'logLevel',
		alias: 'g',
		type: String,
		typeLabel: '{underline LogLevel}',
		description: 'Logging level, valid levels in increasing severity are debug/verbose/info/warn/error.'
	},
	{
		name: 'remoteHost',
		alias: 'r',
		type: String,
		typeLabel: '{underline RemoteHostName}',
		description: 'The remote host name to connect to for Stratum.'
	},
	{
		name: 'remotePort',
		alias: 'o',
		type: Number,
		typeLabel: '{underline RemotePort}',
		description: 'The remote port to connect to for Stratum.'
	},
	{
		name: 'workRoute',
		alias: 'w',
		type: String,
		typeLabel: '{underline WorkRoute}',
		description: 'The route to use for the GetWork endpoint.'
	}
];

function createStartupOptions() {
	const options = commandLineArgs(commandLineOptions);

	if (!options.hasOwnProperty('listenIp')) {
		options.listenIp = '127.0.0.1';
	}
	if (!options.hasOwnProperty('listenPort')) {
		options.listenPort = 8080;
	}
	if (options.hasOwnProperty('logFile')) {
		options.logFile = options.logFile.replace('[DATE]','%DATE%');
	}
	if (!options.hasOwnProperty('logLevel')) {
		options.logLevel = 'info';
	}
	if (!options.hasOwnProperty('remoteHost')) {
		options.remoteHost = 'stratum.amoveopool.com';
	}
	if (!options.hasOwnProperty('remotePort')) {
		options.remotePort = 8822;
	}
	if (!options.hasOwnProperty('workRoute')) {
		options.workRoute = '/work';
	}

	return options;
}

function displayHelpMessage() {
	const commandLineHelpInfo = [
		{
			header: 'VEO GetWork->Stratum',
			content: 'GetWork to Stratum proxy for Amoveo mining.'
		},
		{
			header: 'Options',
			optionList: commandLineOptions
		},
		{
			header: 'Examples',
			content: [
				{
					name: 'Default Behavior',
					summary: 'veo-gs --listenIp=127.0.0.1 --listenPort=8080 --logLevel=info --remoteHost=stratum.amoveopool.com --remotePort=8822 --workRoute=/work'
				},
				{
					name: 'Listen on all local IPs',
					summary: 'veo-gs --listenIp=0.0.0.0'
				},
				{
					name: 'Set GetWork port',
					summary: 'veo-gs --listenPort=5555'
				},
				{
					name: 'Use empty GetWork route',
					summary: 'veo-gs --workRoute=/'
				},
				{
					name: 'Write logging output to file',
					summary: 'veo-gs --logFile=veo-gs-log-[DATE].log'
				}
			]
		}
	];

	console.log(commandLineUsage(commandLineHelpInfo));
}

const startupOptions = createStartupOptions();

if (startupOptions.hasOwnProperty('help') && startupOptions.help === true) {
	displayHelpMessage();
} else {
	loggerFactory.reconfigureLoggers(startupOptions.logLevel, startupOptions.logFile);

	const getWorkServer = new GetWorkServer(false, false, startupOptions.listenIp, startupOptions.listenPort, startupOptions.remoteHost, startupOptions.remotePort, startupOptions.workRoute);
	getWorkServer.start();
}